
#include <iostream>
#include <sstream>
#include <ctime>
#include <string>
#include <chrono>
#include <iomanip>

#include "utils/Logger.h"

namespace talk
{
    std::string getCurrentTimeStamp()
    {
        // get current time
        auto currentTime = std::chrono::system_clock::now();
        // get milliseconds
        auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime.time_since_epoch()) % 1000;

        auto currentTimePoint = std::chrono::system_clock::to_time_t(currentTime);

        // output the time stamp
        std::ostringstream stream;
        // add support for lower level GCC
#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__)) && !defined(__MINGW32__)
        stream << std::put_time(std::localtime(&currentTimePoint), "%T");
#else
        char buffer[80];
        auto success = std::strftime(buffer, 80, "%T", std::localtime(&currentTimePoint));
        assert(0 != success);
        stream << buffer;
#endif

        stream << '.' << std::setfill('0') << std::setw(3) << milliseconds.count();

        return stream.str();
    }

    const std::string LOG_LEVEL[] =
    {
            "DEBUG",
            "CONFIG",
            "INFO",
            "WARNING",
            "ERROR"
    };

    ThreadSafeLogger* ThreadSafeLogger::getInstance()
    {
        static ThreadSafeLogger tLogger(DEBUG);
        return &tLogger;
    }

    ThreadSafeLogger::ThreadSafeLogger(LogLeveL eLevel) : _pFileStream(nullptr), _shutdown(false)
    {
        _eLevel = eLevel;
        initializeFileStream();
        _mLogThread = std::thread(&ThreadSafeLogger::writeThread, this);
    }


    ThreadSafeLogger::~ThreadSafeLogger() {
        _bQuit = true;

        if (_mLooperThread.joinable()) {
            _mLooperThread.join();    
        }

        if (nullptr != _pFileStream) {
            _pFileStream->close();
            delete _pFileStream;
            _pFileStream = nullptr;
        }
    }

    void ThreadSafeLogger::setLogLevel(LogLeveL, eLevel) 
    {
        _eLevel = eLevel;
    }

    LogLeveL ThreadSafeLogger::getLogLevel()
    {
        return _eLevel;
    }

    void ThreadSafeLogger::initializeFileStream() {
        std::string sLogName = "logs/Log_" + getCurrentTimeStamp() + ".log";
        // Initialize file stream
        _pFileStream = new std::ofstream();
        std::ios_base::openmode mode = std::ios_base::out;
        mode |= std::ios_base::trunc;
        _pFileStream->open(fileName, mode);

        // Error handling
        if (!_pFileStream->is_open()) {
            // Print error information
            std::ostringstream sError;
            sError << "FATAL ERROR:  could not Open log file: [" << fileName << "]";
            sError << "\n\t\t std::ios_base state = " << _pFileStream->rdstate();
            std::cerr << sError.str().c_str() << std::endl << std::flush;
            _pFileStream->close();
            delete _pFileStream;
            _pFileStream = nullptr;
        }
    }
    
    void ThreadSafeLogger::writeLog(LogLeveL  eLevel, const std::string &sLog) {
        if (eLevel < _eLevel)
            return;

        std::ostringstream stream;
        stream << getCurrentTimeStamp()
        << " [" << PRIORITY_STRING[eLevel] << "] "
        << sLog;

        _qQueue.push(stream.str());
    }

    void ThreadSafeLogger::writeThread() {
        while (!_bQuit) {
            std::string sLog;
            _qQueue.waitAndPop(sLog);

            std::cout << sLog << std::endl;

            if (_fileStream)
                *_fileStream << sLog << std::endl;
        }
    }

}