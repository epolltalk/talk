/*
Socket Wrapper for native socket
*/
#ifndef __SOCKET_H
#define __SOCKET_H

#ifdef WIN32
#include "win32/net_win32.h"
#else
#include "linux/net_linux.h"
#include <unistd.h>
#endif

using namespace std;

namespace talk
{
    class Socket {
        public:
            Socket() : _iFD(INVALID_SOCKET)
            {}

            Socket(SocketFD iFD) : _iFD(iFD)
            {}

            virtual ~Socket()
            {
                #ifdef WIN32
                    closesocket(_iFD);
                #else
                    ::close(_iFD);
                #endif
            }

            SocketFD GetSocketFD () const {return _iFD;}
            void SetNativeSocket(SocketFD iFD) { _iFD = iFD; }
        private:
            SocketFD _iFD;

    }
}
