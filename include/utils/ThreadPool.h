#ifndef __THREADPOOL_H
#define __THREADPOOL_H

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>

#include "utils/ThreadSafeQueue.h"

#define DEFAULT_THREAD_NUM 10

using namespace std;

namespace talk {
    template <typename Task>
    class ThreadPool {
        ThreadPool &operator=(const ThreadPool &) = delete;
        ThreadPool(const ThreadPool&other) = delete;

    public:
        ThreadPool(size_t iThreadNums, function<void(Type& tTask)> handler);
        virtual ~ThreadPool();
        void submit(const Task& task);
        void submit(shared_ptr<Task> pTask);
        void terminate(){_bQuit = true;};

    private:
        bool _bQuit;
        size_t _iNums;
        function<void(Type& tTask)> _fHandler;
        vector<thread> _vWorkers;
        ThreadSafeQueue<Task> _qTaskQueue;
    };

}

#include "template/ThreadPool.tcc"

#endif //TALK_THREADPOOL_H
