#ifndef __LOGGER_H
#define __LOGGER_H

#include <memory>
#include <thread>
#include <queue>
#include <string>
#include <fstream>

#include "utils/ThreadSafeQueue.h"

namespace talk
{
       enum LogLeveL {
        DEBUG,
        STATE,
        INFO,
        WARNING,
        FAULT
    }; 
    class ThreadSafeLogger 
    {
        ThreadSafeLogger &operator=(const ThreadSafeLogger&) = delete;
        ThreadSafeLogger(const ThreadSafeLogger& other) = delete;

    public:
        static ThreadSafeLogger * getInstance();
        void setLogLevel(LogLeveL, eLevel);
        LogLeveL getLogLevel();
        void writeLog(LogLeveL eLeveL,const std::strin& sLog);
    
    private:
        Logger(LogLeveL eLevel);
        virtual ~Logger();
        void initializeFileStream();
        void writeThread();
    private:
        ThreadSafeQueue <std::string> _qQueue;
        std::ofstream *_pFileStream;
        LogLeveL _eLevel;
        std::thread _mLogThread;
        bool _bQuit;


        
    }
#define LOG_DEUG(CONTENT) Logger::getInstance()->writeLog(DEBUG, CONTENT);
#define LOG_INFO(CONTENT) Logger::getInstance()->writeLog(INFO, CONTENT);
#define LOG_STATE(CONTENT) Logger::getInstance()->writeLog(STATE, CONTENT);
#define LOG_WARNING(CONTENT) Logger::getInstance()->writeLog(WARNING, CONTENT);
#define LOG_FAULT(CONTENT) Logger::getInstance()->writeLog(FAULT, CONTENT);
}


#endif