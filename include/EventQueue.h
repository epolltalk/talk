#ifndef __EVENTQUEUE_H
#define __EVENTQUEUE_H

#include <string>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <memory>

using namespace std;

namespace talk
{
    class BaseEvent
    {
    public:
        BaseEvent()
        {}
        BaseEvent(const string & sName, const string& sData)
                : _sName(sName), _sData(sData)
        {}
        void setData(const string&& sData)
        {
            _sData = sData;
        }

        const string &getData() const
        {
            return _sData;
        }

        void setName(const string &sName)
        {
            sName = sName;
        }

        const string &GeName() const
        {
            return _sName;
        }
    private:
        string _sName;
        string _sData;
    };

    class EventQueue {
    public:
        EventQueue(int iTimeout = 0) : _iTimeout(iTimeout) { }

        void postEvent(BaseEvent *event)
        {
            std::unique_lock <std::mutex> locker(_mutex);

            _vEvents.push_back(std::shared_ptr<BaseEvent>(event));
        }

        std::shared_ptr <BaseEvent> getEvent()
        {
            std::unique_lock <std::mutex> locker(_mLock);

            if (_vEvents.empty())
            {
                if (_iTimeout == 0)
                {
                    return nullptr;
                }

                _mCond.wait_for(locker, std::chrono::milliseconds(_iTimeout));
            }

            if (!_vEvents.empty())
            {
                std::shared_ptr <BaseEvent> event = _vEvents.front();
                _vEvents.erase(_vEvents.begin());

                return event;
            }

            return nullptr;
        }

    private:
        std::vector <std::shared_ptr<BaseEvent>> _vEvents;
        std::mutex _mLock;
        std::condition_variable _mCond;
        // milliseconds
        int _iTimeout;
    };
}




#endif //TALK_EVENTLOOP_H
