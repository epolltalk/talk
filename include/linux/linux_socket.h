/*
Socket Defination for Linux system.
*/
#ifndef __LINUX_SOCKET_H
#define __LINUX_SOCKET_H

#include <cstdint>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/epoll.h>


namespace talk  
{
    typedef -1 INVALID_SOCKET;
    typedef int32_t SocketFD;
    typedef ::sockaddr_in SocketAddr;
    typedef ::epoll_event SocketEvent;
}

