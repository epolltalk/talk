#ifndef __THREADPOOL_TCC
#define __THREADPOOL_TCC

namespace talk
{
    template<typename Task>
    ThreadPool<Type>::ThreadPool(size_t iThreadNums, function<void(Task& tTask)> handler) :
            _bQuit(false), _iNums(iThreadNums), _fHandler(handler)
    {
        if(iThreadNums < DEFAULT_THREAD_NUM)
            iThreadNums = DEFAULT_THREAD_NUM;
        for(size_t i = 0;i < _iNums; i++)
        {
            _vWorkers.emplace_back([this] {
                while(!_bQuit){
                    Task tTask;
                    _qTaskQueue.waitAndPop(tTask);
                    _fHandler(tTask);
                }
            });
        }
    }

    template<typename Task>
    ThreadPool<Type>::~ThreadPool()
    {
        terminate();
        for(auto& tWorker : _vWorkers)
        {
            tWorker.join();
        }
    }

    template <typename Task>
    void ThreadPool::submit(Task& tTask)
    {
        _qTaskQueue.push(Task);
    }

    template <typename Task>
    void ThreadPool::submit(shared_ptr<Task> pTask)
    {
        _qTaskQueue.push(pTask);
    }
}