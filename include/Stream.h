/*
Interface for all class has read/write function
*/
#include <string>

namespace talk
{
class Stream 
{
    virtual int32_t receive(char* pBuffer, int32_t iLen, int32_t& ireadLen) = 0;
    virtual int32_t send(const string& sSendBuffer) = 0;
    virtual void setStreamSink(DataSink* tDataSink) = 0;
    virtual DataSink* GetStreamSink() = 0;
    

}
}
