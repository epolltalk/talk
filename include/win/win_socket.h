#ifndef __WIN_SOCKET_H
#define __WIN_SOCKET_H

#include <WinSock2.h>

namespace talk 
{

	typedef SOCKET socketFD;
	typedef SOCKADDR_IN socketAddr;
#define CloseSocket(socket) closesocket(socket);

	class WindowsSocketInitializer {
	public:
		static void Initialize();

	private:
		WindowsSocketInitializer();
	};
}